#!/usr/bin/env python
# coding: utf-8

# # Continued from Code 1: Analysis at population level
# 
# # Code 4: Analysis at patient level 
# 
# ### Created 8/4/19; Last updated 10/27/19
# ### Student: Shradha Mukherjee, PhD
# ### External Advisor, Site Preceptor: William Hayes, PhD 
# ### Faculty Project Advisor: Robert Greenes, MD, PhD

# # Step1A_part1 (manually done): To obtain list of NSCLC patient genes and gene variants. 
# 
# Prototype for NSCLC Patient4: LUC1 patient validated variant data from Govindan_etal_Cell2012-main.
# ![alt text](./screenshots/Patient4_1_paper_image1.png "image1_Patient4")
# 
# Generalization: Any patient variant data for a given disease that has been annotated to find mutations that might be associated with diseases can be usef here. 
# Click 'Edit' tab on jupyter notebook --> click 'Find replace' to replace Patient4 with patientID and NSCLC with disease of interest.
# 
# ProTip: use position converter to format variant position to those required by snp-nexus using tool https://mutalyzer.nl/position-converter  
# ![alt text](./screenshots/mutalyzer_image1.png "image1_mutalyzer")
# 
# ProTip: The variants can be annotated on SNP_nexus_tool_NucleicAcidRes.2018 and the resulting output file can be used here. Input formats acceptable on SNP_nexus can be found at https://snp-nexus.org/guide.html#input_format In the present case the format <"chromosome" "chromosome number or id"  "position" "reference_nucleotide" "variant or observed nucleotide" "chromosome strand"> was used 
# Example: "Chromosome" "1" "178153788" "G"  "T" "1"
# 
# ![alt text](./screenshots/snp_nexus_image1.png "image1_snp_nexus")
# ![alt text](./screenshots/snp_nexus_image2.png "image2_snp_nexus")
# ![alt text](./screenshots/snp_nexus_image3.png "image3_snp_nexus")

# In[1]:


#############################################################################################################################


# ## Steps below are automated for patients with disease 'NSCLC' ###################################

# In[2]:


#############################################################################################################################


# In[3]:


import os


# In[4]:


#get current working directory
cwd=os.getcwd()
cwd


# In[5]:


#to change current working direectory uncomment line below
#os.chdir("/path/to_new_directory")


# In[6]:


#create directories to put results
if not os.path.exists('OutputBELformat'):
    os.makedirs('OutputBELformat')
    
if not os.path.exists('OutputDefaultFormat'):
    os.makedirs('OutputDefaultFormat')
    
if not os.path.exists('OutputNanoPubsFormat'):
    os.makedirs('OutputNanoPubsFormat')
    
if not os.path.exists('OutputSummaryPlots'):
    os.makedirs('OutputSummaryPlots')
    
if not os.path.exists('OutputSummaryTables'):
    os.makedirs('OutputSummaryTables')


# In[7]:


#list all files in the current directory
os.listdir('.')


# In[8]:


import pandas as pd
import numpy as np


# # Step1A_part2 of workflow continued (automated): To import patient genes and gene variants prepared in Step1A_part1

# In[9]:


#assign spreadsheat filename to file
file1='./InputFromUser/Step1A_TableS3_LUC1toLUC17_SNP_NSCLC_Govindan_etal_Cell2012_hg18.xlsx'
#load spreadsheet
x1=pd.ExcelFile(file1)
#print sheet names
print(x1.sheet_names)


# In[10]:


#we have only one sheet we will load this into a dataframe
df1=x1.parse('singleToTriple_AA_2_pat4')


# In[11]:


#let's look at the number of rows and columns in the dataframe
df1.shape


# In[12]:


#let's look at the first few lines of the dataframe
df1.head()
#df1.head(3)


# In[13]:


#lets view the headers or colnames in the data frame
list(df1.columns)


# In[14]:


#let's look at the number of rows and columns in the dataframe
df1.shape


# In[15]:


#now we select and keep only the gene names that we need 
df1Genes=df1[['Gene']]
#lets few the first few Genes
df1Genes.head()


# In[16]:


df1GenesList=df1Genes.values.T.tolist()
type(df1GenesList)
df1GenesValues=df1GenesList[0]


# In[17]:


df1GenesValues1=list(set(df1GenesValues))
#remove nan i.e. NA values
df1GenesValues2=[x for x in df1GenesValues1 if str(x) != 'nan']
#reassign to original dataframe name
df1GenesValues=df1GenesValues2
df1GenesValues


# # Step1B of workflow: EnrichR Data: To overlap patient genes with population genes EnrichR results

# #### now we import the population level gene GO information to overlap

# In[18]:


#assign spreadsheat filename to file
file2='./OutputBELformat/Step1B_enrichrGO_results_forBEL_gene-gene-interactions_NSCLC_Population.xlsx'
#load spreadsheet
x2=pd.ExcelFile(file2)
#print sheet names
print(x2.sheet_names)


# In[19]:


#we have only one sheet we will load this into a dataframe
df_enrichrGO_Genes_pop=x2.parse('enrichrGO_BEL')


# In[20]:


#let's look at the number of rows and columns in the dataframe
df_enrichrGO_Genes_pop.shape


# In[21]:


#let's look at the first few lines of the dataframe
df_enrichrGO_Genes_pop.head()
#df_enrichrGO_Genes_pop.head(3)


# In[22]:


#lets view the headers or colnames in the data frame
list(df_enrichrGO_Genes_pop.columns)


# #### now we select and keep only the gene names that are in patient 

# In[23]:


df_enrichrGO_Genes_pat4=df_enrichrGO_Genes_pop[df_enrichrGO_Genes_pop.HGNC.isin(df1GenesValues)]
#lets few the first few Genes
df_enrichrGO_Genes_pat4.head()


# In[24]:


#let's look at the number of rows and columns in the dataframe
df_enrichrGO_Genes_pop.shape, df_enrichrGO_Genes_pat4.shape
#we lost some genes in patient because patient does not have all pop genes


# In[25]:


#we will export this dataframe to a excel data format
df_enrichrGO_Genes_pat4.to_excel("./OutputBELformat/Step1B_enrichrGO_results_forBEL_gene-gene-interactions_NSCLC_Patient4.xlsx", sheet_name='enrichrGO_BEL')

#uncomment line below to export as csv file
#Step1C_csv=df_enrichrGO_Genes_pat4.to_csv("Step1B_enrichrGO_results_forBEL_gene-gene-interactions_NSCLC_Patient4.csv", index=False)


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[26]:


df_enrichrGO_Genesnp=df_enrichrGO_Genes_pat4


# In[27]:


#now we rename columns giving them specific names that are available on BEL namespace
df_enrichrGO_Genesnp=df_enrichrGO_Genesnp.rename(columns={'HGNC':'subject', 'relation':'relation',  'GO':'obj', 'citations':'citations', 'database_url':'db_url'})
#change column order
df_enrichrGO_Genesnp=df_enrichrGO_Genesnp[['subject', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_enrichrGO_Genesnp.head()


# In[28]:


#Export results as csv to use in next step
df_enrichrGO_Genesnp.to_csv("./OutputNanoPubsFormat/Step1B_enrichrGO_results_forBELnp_gene-gene-interactions_NSCLC_Patient4.csv")


# In[29]:


#William Hayes function modified for making enrichR GO gene-gene-effect nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pat4_v1"

#define function
def process_step1b_file():
    source = "https://amp.pharm.mssm.edu/Enrichr/"
    fn = "./OutputNanoPubsFormat/Step1B_enrichrGO_results_forBELnp_gene-gene-interactions_NSCLC_Patient4.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, relation, obj, citation, db_url) = row
            nanopub = {}
            subj = f"p(HGNC:{subject})"
            obj = f"bp(GO:{obj})"         


            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_enrichR_GO_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": citation},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "database":{"uri_db": db_url},
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pat4_v1",
                            "gd_creator": creator,
                            "note": f"{subject} acts as {relation} of {obj}. This relation may or may not hold true in the context of NSCLC.",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1b_file()

    with open("./OutputNanoPubsFormat/Step1B_enrichrGO_results_forBELnp_gene-gene-interactions_NSCLC_Patient4.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # Step1C of workflow: DGIdb Data: To overlap patient genes with population genes DGIdb results

# #### now we import the population level gene DGIdb information to overlap

# In[30]:


#assign spreadsheat filename to file
file3='./OutputDefaultFormat/Step1C_DGIdb_results_drug-gene-interactions_NSCLC_Population.xlsx'
#load spreadsheet
x3=pd.ExcelFile(file3)
#print sheet names
print(x3.sheet_names)


# In[31]:


#we have only one sheet we will load this into a dataframe
df_DGIdb_pop=x3.parse('DGIdb')


# In[32]:


#let's look at the number of rows and columns in the dataframe
df_DGIdb_pop.shape


# In[33]:


#let's look at the first few lines of the dataframe
df_DGIdb_pop.head()
#df_enrichrGO_Genessel_pop.head(3)


# In[34]:


#lets view the headers or colnames in the data frame
list(df_DGIdb_pop.columns)


# #### now we select and keep only the gene names that are in patient 

# In[35]:


df_DGIdb_pat4=df_DGIdb_pop[df_DGIdb_pop.geneName.isin(df1GenesValues)]
#lets few the first few Genes
df_DGIdb_pat4.head()


# In[36]:


#let's look at the number of rows and columns in the dataframe
df_DGIdb_pop.shape, df_DGIdb_pat4.shape
#we lost some genes in patient because patient does not have all pop genes


# In[37]:


#we will export this dataframe to a excel data format
df_DGIdb_pat4.to_excel("./OutputDefaultFormat/Step1C_DGIdb_results_drug-gene-interactions_NSCLC_Patient4.xlsx", sheet_name='DGIdb')

#uncomment line below to export as csv file
#df_DGIdb_csv=r_norm.to_csv("Step1C_DGIdb_results_drug-gene-interactions_NSCLC.csv", index=False)


# #### Now we will convert df_DGIdb to BEL statement components and nanopubs to load to BioDati Studio

# In[38]:


df_DGIdb=df_DGIdb_pat4


# In[39]:


#let's look at the number of rows and columns in the dataframe
df_DGIdb.shape


# In[40]:


#let's look at the first few lines of the dataframe
df_DGIdb.head(3)


# In[41]:


#in the 'interactionType' column some values have multiple synonyms, which we want to replace with a single BEL v 2.1.0 availabel relation term
#so lets first look at the different types of values present in the interaction_types column
df_DGIdb.interactionTypes.unique()


# In[42]:


#Using available BEL2.1.0 'predicate' or relationship options we will replace 
#the values of interactionTypes to valid BEL relationship 
#you can view the relationships here: https://language.bel.bio/language/reference/2.1.0/relations/ 
#create a dictionary
x_remap = {'agonist': 'increases', 
        'positive allosteric modulator': 'increases',  
        'activator': 'increases', 
        'agonist,allosteric modulator': 'increases', 
        'stimulator': 'increases', 
        'inducer': 'increases' }

y_remap = {'inhibitor':'decreases',
        'antagonist':'decreases',
        'blocker':'decreases',
        'channel blocker':'decreases',
        'antisense':'decreases',
        'inverse agonist':'decreases',
        'gating inhibitor':'decreases',
        'negative modulator':'decreases',
        'antisense oligonucleotide':'decreases',
        'allosteric modulator,antagonist':'decreases',
        'channel blocker,gating inhibitor':'decreases',
        'antagonist,inhibitor':'decreases',
        'inhibitory allosteric modulator':'decreases',
        'suppressor':'decreases'}
    
z_remap = {'binder': 'association', 
        'modulator': 'regulates', 
        'antibody': 'association', 
        'allosteric modulator': 'regulates', 
        'vaccine': 'association', 
        'activator,antagonist': 'regulates', 
        'cofactor': 'association', 
        'activator,channel blocker': 'regulates', 
        'partial agonist': 'regulates', 
        'agonist,antagonist': 'regulates'}


# In[43]:


df_DGIdb.replace({"interactionTypes": x_remap})
df_DGIdb.replace({"interactionTypes": y_remap})
df_DGIdb.replace({"interactionTypes": z_remap})

#replace 'NaN' or unspecified interactionType value with 'association'
df_DGIdb_pat4['interactionTypes'].fillna('association', inplace=True)

#lets few the first few rows
df_DGIdb_pat4.head()


# In[44]:


#lets view the headers or colnames in the data frame
list(df_DGIdb.columns)


# In[45]:


#now we select and keep only those columns that we need 
#df2sel=df2[['gene_name','drug_name','interaction_types','PMIDs']]
df_DGIdbsel=df_DGIdb[['drugChemblId','interactionTypes','geneName', 'pmids_one_url','interactionId_url', 'drugName', 'score']]
#'drugName' and score' are saved to prepare summary file not for BEL

#alternative 2
#df2.loc[:,'gene':'pmids']
df_DGIdbsel.head()


# In[46]:


#now we rename columns giving them specific names that are available on BEL namespace.
df_DGIdbsel=df_DGIdbsel.rename(columns={'drugChemblId':'CHEBI','interactionTypes':'relation','geneName':'HGNC', 'pmids_one_url':'citations','interactionId_url':'database_url','drugName':'drugName', 'score':'score'})

#lets few the first few rows
df_DGIdbsel.head()


# In[47]:


#in the 'interaction_type' column some values have multiple synonyms, which we want to replace with a single BEL v 2.1.0 availabel relation term
#so lets first look at the different types of values present in the interaction_types column
df_DGIdbsel.relation.unique()


# In[48]:


#we will export this dataframe to a excel data format
df_DGIdbsel.to_excel("./OutputBELformat/Step1C_DGIdb_results_forBEL_drug-gene-interactions_NSCLC_Patient4.xlsx", sheet_name='DGIdb_BEL')

#uncomment line below to export as csv file
#Step1C_csv=df_DGIdbsel.to_csv("Step1C_DGIdb_results_ForBEL_drug-gene-interactions_NSCLC.csv", index=False)


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[49]:


df_DGIdbselnp=df_DGIdbsel
#database url is same as db_url, will be renamed in the next step


# In[50]:


#now we rename columns giving them specific names that are available on BEL namespace
df_DGIdbselnp=df_DGIdbselnp.rename(columns={'CHEBI':'subject', 'relation':'relation',  'HGNC':'obj', 'citations':'citations', 'database_url':'db_url'})
#change column order and do not use 'drugName' and 'score' which are not for BEL
df_DGIdbselnp=df_DGIdbselnp[['subject', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_DGIdbselnp.head()


# In[51]:


#Export results as csv to use in next step
df_DGIdbselnp.to_csv("./OutputNanoPubsFormat/Step1C_DGIdb_results_forBELnp_drug-gene-interactions_NSCLC_Patient4.csv")


# In[52]:


#William Hayes function modified for making drug-gene-effect nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pat4_v1"

#define function
def process_step1d_file():
    source = "http://www.dgidb.org/"
    fn = "./OutputNanoPubsFormat/Step1C_DGIdb_results_forBELnp_drug-gene-interactions_NSCLC_Patient4.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, relation, obj, citation, db_url) = row
            nanopub = {}
            subj = f"a(CHEBI:{subject})"
            obj = f"p(HGNC:{obj})"         


            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_DGIdb_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": citation},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "database":{"uri_db": db_url},
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pat4_v1",
                            "gd_creator": creator,
                            "note": f"{subject} acts as {relation} of {obj}. This relation may or may not hold true in the context of NSCLC.",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1d_file()

    with open("./OutputNanoPubsFormat/Step1C_DGIdb_results_forBELnp_drug-gene-interactions_NSCLC_Patient4.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # Step1D of workflow continued (automated): ClinVar Data: To overlap patient genes with population genes ClinVar results

# #### now we import the population level gene ClinVar information to overlap

# In[53]:


#assign spreadsheat filename to file
file4='./OutputDefaultFormat/Step1D_ClinVar_results_gene-variants-effects_NSCLC_Population.xlsx'
#load spreadsheet
x4=pd.ExcelFile(file4)
#print sheet names
print(x4.sheet_names)


# In[54]:


#we have only one sheet we will load this into a dataframe
df_ClinVar_pop=x4.parse('ClinVar')


# In[55]:


#let's look at the number of rows and columns in the dataframe
df_ClinVar_pop.shape


# In[56]:


#let's look at the first few lines of the dataframe
df_ClinVar_pop.head()
#df_enrichrGO_Genessel_pop.head(3)


# In[57]:


#lets view the headers or colnames in the data frame
list(df_ClinVar_pop.columns)


# #### now we select and keep only the gene names that are in patient 

# In[58]:


df_ClinVar_pat4=df_ClinVar_pop[df_ClinVar_pop.GeneSymbol.isin(df1GenesValues)]
#lets few the first few Genes
df_ClinVar_pat4.head()


# In[59]:


#let's look at the number of rows and columns in the dataframe
df_ClinVar_pop.shape, df_ClinVar_pat4.shape
#we lost some genes in patient because patient does not have all pop genes


# In[60]:


#we will export this dataframe to a excel data format
df_ClinVar_pat4.to_excel("./OutputDefaultFormat/Step1D_ClinVar_results_gene-variants-effects_NSCLC_Patient4.xlsx", sheet_name='ClinVar')

#uncomment line below to export as csv file
#df_ClinVar_pat4_csv=df_ClinVarNew.to_csv("Step1D_ClinVar_results_gene-variants-effects_NSCLC.csv", index=False)


# #### Now we will convert df_ClinVarNew to BEL statement components and nanopubs to load to BioDati Studio

# In[61]:


df_ClinVar=df_ClinVar_pat4


# In[62]:


#now we select and keep only those columns that we need 
#df2sel=df2[['GeneSymbol','ClinicalSignificance','Name','Link']]
df_ClinVarsel=df_ClinVar[['GeneSymbol','ClinicalSignificance','Name','Link']]

#alternative 2
#df2.loc[:,'gene':'pmids']
df_ClinVarsel.head()


# In[63]:


#filter dataframe to keep only 'Pathogenic'
df_ClinVarsel_1 = df_ClinVarsel[df_ClinVarsel['ClinicalSignificance']=='Pathogenic']
#notice after filtering we have fewer entries
print(df_ClinVarsel.shape, df_ClinVarsel_1.shape)


# In[64]:


#Pathogenic is not a valid BEL relationship (you can view the relationships here: https://language.bel.bio/language/reference/2.1.0/relations/ 
#The relation in the nanopubs should probably be 'increases', assuming increases in that gene variant increases lung cancer - there could also be decreases or unknown. 
#If unknown, then it's either 'association' or more informative and preferred:  positiveCorrelation or negativeCorrelation.

#so here we replace value 'Pathogenic' with 'increases'
df_ClinVarsel_1['ClinicalSignificance'].replace(['Pathogenic'],['increases'],inplace=True) 


# In[65]:


#reassign to original variable name
df_ClinVarsel=df_ClinVarsel_1
#lets view the new dataframe
df_ClinVarsel.head()


# In[66]:


print(df_ClinVarsel.shape)


# In[67]:


#lets view the headers or colnames in the data frame
list(df_ClinVarsel.columns)


# In[68]:


#diseases can be defines by MESH namespace 'MESH:"Carcinoma, Non-Small-Cell Lung"'
df_ClinVarsel['Disease']='MESH:"Carcinoma, Non-Small-Cell Lung"'
#now we rename columns giving them specific names that are available on BEL namespace
df_ClinVarsel=df_ClinVarsel.rename(columns={'GeneSymbol':'HGNC', 'Name':'Variant', 'ClinicalSignificance':'relation', 'Disease':'Disease', 'Link':'citations'})
#change column order
df_ClinVarsel=df_ClinVarsel[['HGNC', 'Variant', 'relation', 'Disease', 'citations']]
#lets view the new dataframe
df_ClinVarsel.head()


# In[69]:


#uncomment line below to export results as csv file 
#df_ClinVar_pat4sel_csv=df_ClinVarsel_BioDati_ready.to_csv("Step1D_ClinVar_results_forBEL_gene-variants-effects_NSCLC.csv", index=False)

#we will export this dataframe to a excel data format
df_ClinVarsel.to_excel("./OutputBELformat/Step1D_ClinVar_results_forBEL_gene-variants-effects_NSCLC_Patient4.xlsx", sheet_name='ClinVarBEL')


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[70]:


df_ClinVarselnp=df_ClinVarsel
#citation url will also be used as database url as its the same for these entries
df_ClinVarselnp['db_url']=df_ClinVarselnp['citations']


# In[71]:


#now we rename columns giving them specific names that are available on BEL namespace
df_ClinVarselnp=df_ClinVarselnp.rename(columns={'HGNC':'subject', 'Variant':'variant', 'relation':'relation',  'Disease':'obj', 'citations':'citations', 'db_url':'db_url'})
#change column order
df_ClinVarselnp=df_ClinVarselnp[['subject', 'variant', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_ClinVarselnp.head()


# In[72]:


#Export results as csv to use in next step
df_ClinVarselnp.to_csv("./OutputNanoPubsFormat/Step1D_ClinVar_results_forBELnp_gene-variants-effects_NSCLC_Patient4.csv")


# In[73]:


#William Hayes function for making gene-variant-effects nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pat4_v1"

#define function
def process_step1d_file():
    source = "https://www.ncbi.nlm.nih.gov/clinvar/"
    fn = "./OutputNanoPubsFormat/Step1D_ClinVar_results_forBELnp_gene-variants-effects_NSCLC_Patient4.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, variant, relation, obj, citation, db_url) = row
            nanopub = {}
            matches = re.search(r"^([\w\_\.]+)\(.*\(([\w\.]+)\)", variant)
            if matches:
                subj = f"p(REFSEQ:{matches.group(1)}, var({matches.group(2)}))"
            else:
                subj = f"BADMATCH - {variant}"

            obj = f"path({obj})"

            matches = re.match(r".*?\"(.*?)\"", citation)
            if matches:
                uri = matches.group(1)
            else:
                uri = f"BADMATCH - {citation}"

            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_clinvar_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": uri},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pat4_v1",
                            "gd_creator": creator,
                            "note": f"{subject} mutation increases non-small cell lung cancer",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1d_file()

    with open("./OutputNanoPubsFormat/Step1D_ClinVar_results_forBELnp_gene-variants-effects_NSCLC_Patient4.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # Step1E of workflow continued (manually done): PC Viz Data: To overlap patient genes with population genes PC Viz results

# #### now we import the population level gene PC Viz information to overlap

# In[74]:


#assign spreadsheat filename to file
file5='./OutputDefaultFormat/Step1E_PCViz_results_gene-gene-interactions_NSCLC_Population.xlsx'
#load spreadsheet
x5=pd.ExcelFile(file5)
#print sheet names
print(x5.sheet_names)


# In[75]:


#we have only one sheet we will load this into a dataframe
df_PCViz_pop=x5.parse('PCViz')


# In[76]:


#let's look at the number of rows and columns in the dataframe
df_PCViz_pop.shape


# In[77]:


#let's look at the first few lines of the dataframe
df_PCViz_pop.head()
#df_enrichrGO_Genessel_pop.head(3)


# In[78]:


#lets view the headers or colnames in the data frame
list(df_PCViz_pop.columns)


# #### now we select and keep only the gene names that are in patient 

# In[79]:


df_PCViz_pat4=df_PCViz_pop[df_PCViz_pop.gene.isin(df1GenesValues)]
#lets few the first few Genes
df_PCViz_pat4.head()


# In[80]:


#let's look at the number of rows and columns in the dataframe
df_PCViz_pop.shape, df_PCViz_pat4.shape
#we lost some genes in patient because patient does not have all pop genes


# In[81]:


#we will export this dataframe to a excel data format
df_PCViz_pat4.to_excel("./OutputDefaultFormat/Step1E_PCViz_results_gene-gene-interactions_NSCLC_Patient4.xlsx", sheet_name='PCViz')

#uncomment line below to export as csv file
#df_PCVizNew_csv=df_PCVizNew.to_csv("Step1E_PCViz_results_gene-gene-interaction_NSCLC.csv", index=False)


# #### Now we will convert df_PCVizNew to BEL statement components and nanopubs to load to BioDati Studio

# In[82]:


df_PCVizNew=df_PCViz_pat4


# In[83]:


#now we select and keep only those columns that we need, here we need all the original columns 
#df2sel=df2[['gene', 'interaction', 'target']]
df_PCVizsel=df_PCVizNew[['gene', 'interaction', 'target']]

#alternative 2
#df2.loc[:,'gene':'pmids']
df_PCVizsel.head()


# In[84]:


#in the 'interaction' column there are different types of interactions, but we want to replace with BEL v 2.1.0 availabel relation terms
#so lets first look at the different types of values present in the ClinicalSignificance column
df_PCVizsel.interaction.unique()


# In[85]:


#Using available BEL2.1.0 'predicate' or relationship options we will replace 
#the values of interaction types to valid BEL relationship 
#you can view the relationships here: https://language.bel.bio/language/reference/2.1.0/relations/ 

df_PCVizsel.replace({'in-complex-with': 'hasComponent', 
        'controls-state-change-of': 'regulates',  
        'controls-expression-of': 'regulates', 
        'controls-phosphorylation-of': 'regulates', 
        'controls-transport-of': 'regulates', 
        'chemical-affects': 'regulates' })
df_PCVizsel.head()


# In[86]:


print(df_PCVizsel.shape)


# In[87]:


#lets view the headers or colnames in the data frame
list(df_PCVizsel.columns)


# In[88]:


#the PCViz does not provide citation for each interaction, so we will simply put the pubmed link here. 
#User is encouraged to add specific citations from pubmed to the exported file manually later
df_PCVizsel['Search_Link']='https://www.ncbi.nlm.nih.gov/pubmed'

#now we rename columns giving them specific names that are available on BEL namespace
df_PCVizsel=df_PCVizsel.rename(columns={'gene':'HGNC_1', 'interaction':'relation', 'target':'HGNC_2', 'Search_Link':'citations'})
#change column order
#df_PCVizsel=df_PCVizsel[['HGNC', 'relation', 'HGNC', 'citations']]

#lets view the new dataframe
df_PCVizsel.head()


# In[89]:


#uncomment line below to export results as csv file 
#df_PCVizsel_csv=df_PCVizsel_BioDati_ready.to_csv("Step1E_PCViz_results_forBEL_gene-gene-interaction_NSCLC.csv", index=False)

#we will export this dataframe to a excel data format
df_PCVizsel.to_excel("./OutputBELformat/Step1E_PCViz_results_forBEL_gene-gene-interactions_NSCLC_Patient4.xlsx", sheet_name='PCVizBEL')


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[90]:


df_PCVizselnp=df_PCVizsel
#citation url will also be used as database url as its the same for these entries
df_PCVizselnp['db_url']=df_PCVizselnp['citations']


# In[91]:


#now we rename columns giving them specific names that are available on BEL namespace
df_PCVizselnp=df_PCVizselnp.rename(columns={'HGNC_1':'subject', 'relation':'relation',  'HGNC_2':'obj', 'citations':'citations', 'db_url':'db_url'})
#change column order
df_PCVizselnp=df_PCVizselnp[['subject', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_PCVizselnp.head()


# In[92]:


#Export results as csv to use in next step
df_PCVizselnp.to_csv("./OutputNanoPubsFormat/Step1E_PCViz_results_forBELnp_gene-gene-interactions_NSCLC_Patient4.csv")


# In[93]:


#William Hayes function modified for making gene-gene-interaction nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pat4_v1"

#define function
def process_step1e_file():
    source = "https://www.pathwaycommons.org/pcviz/"
    fn = "./OutputNanoPubsFormat/Step1E_PCViz_results_forBELnp_gene-gene-interactions_NSCLC_Patient4.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, relation, obj, citation, db_url) = row
            nanopub = {}
            subj = f"p(HGNC:{subject})"
            obj = f"p(HGNC:{obj})"         


            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_PCViz_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": citation},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "database":{"uri_db": db_url},
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pat4_v1",
                            "gd_creator": creator,
                            "note": f"{subject} acts as {relation} of {obj}. This relation may or may not hold true in the context of NSCLC.",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1e_file()

    with open("./OutputNanoPubsFormat/Step1E_PCViz_results_forBELnp_gene-gene-interactions_NSCLC_Patient4.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # (automated step) Creating a summary table of results: Gene symbol, common variant, drug to use and GO

# ##### Import the Pop Genes NSCLC most commonly mutated genes data

# In[94]:


#assign spreadsheat filename to file
file0='./InputFromUser/Step1A_MostCommonMutatedGenes.xlsx'
#load spreadsheet
x0=pd.ExcelFile(file0)
#print sheet names
print(x0.sheet_names)


# In[95]:


#we have only one sheet we will load this into a dataframe
df0=x0.parse('Sheet1')


# In[96]:


#let's look at the number of rows and columns in the dataframe
df0.shape


# In[97]:


#let's look at the first few lines of the dataframe
df0.head()
#df0.head(3)


# In[98]:


#lets view the headers or colnames in the data frame
list(df0.columns)


# In[99]:


summary1 = pd.merge(df_DGIdbsel[['CHEBI', 'relation', 'HGNC', 'citations', 'database_url','drugName', 'score']],
                 df_ClinVarsel[['HGNC', 'relation', 'Variant', 'citations', 'Disease']],
                 on='HGNC')
summary1.head()


# In[100]:


#lets view the headers or colnames in the data frame
list(summary1.columns)
#save this to excel
#summary1


# In[101]:


#This second merge gives too many rows so don't use citation and database url for GO
summary2 = pd.merge(df_DGIdbsel[['CHEBI', 'relation', 'HGNC', 'citations', 'database_url','drugName', 'score']],
                 df_enrichrGO_Genes_pat4[['HGNC', 'relation', 'GO']],
                 on='HGNC')
summary2.head()


# In[102]:


#lets view the headers or colnames in the data frame
list(summary2.columns)
#save this to excel
#summary2


# In[103]:


#lets look at the number of known pathogenic variants in these Patient4 NSCLC genes 
FilterByOvClinVarUniqGenes=df_ClinVarsel['HGNC'].value_counts()
#convert series to dataframe
FilterByOvClinVarUniqGenes=FilterByOvClinVarUniqGenes.to_frame().reset_index()
#name columns
FilterByOvClinVarUniqGenes.columns=('HGNC','NoOfVarsKnownToBePathogenicInNSCLCforThisGene')
#prepare Pop genes counts
df1_Pop=df0
df1_Pop.columns=('HGNC','NoOfVarsMutsCasesKnownInNSCLCPopForThisGene')
#merge
summaryVarUniqGenes= pd.merge(df1_Pop, FilterByOvClinVarUniqGenes, on='HGNC')
#save this to excel
#summaryVarUniqGenes


# In[104]:


#lets look at the number of GO terms in these Pop NSCLC genes
FilterByOvGOUniqGenes=df_enrichrGO_Genes_pat4['HGNC'].value_counts()
#convert series to dataframe
FilterByOvGOUniqGenes=FilterByOvGOUniqGenes.to_frame().reset_index()
#name columns
FilterByOvGOUniqGenes.columns=('HGNC','NoOfGOtermsKnownToBeSignificantInNSCLCforThisGene')
#prepare Pop genes counts
df1_Pop=df0
df1_Pop.columns=('HGNC','NoOfVarsMutsCasesKnownInNSCLCPopForThisGene')
#merge 
summaryGOUniqGenes= pd.merge(df1_Pop, FilterByOvGOUniqGenes, on='HGNC')
#save this to excel
#summaryGOUniqGenes


# In[105]:


#view and save plots
import matplotlib.pyplot as plt

#pp = PdfPages('Summary_drug-gene-variants-effects-GO_NSCLC_Population.pdf')
summaryVarUniqGenes.plot(x='HGNC', y='NoOfVarsMutsCasesKnownInNSCLCPopForThisGene', kind='bar', color="C2") 
plt.savefig('./OutputSummaryPlots/Summary1_drug-gene-variants-effects-GO_NSCLC_Patient4.png')
summaryVarUniqGenes.plot(x='HGNC', y='NoOfVarsKnownToBePathogenicInNSCLCforThisGene', kind='bar', color="C3")
plt.savefig('./OutputSummaryPlots/Summary2_drug-gene-variants-effects-GO_NSCLC_Patient4.png')
summaryGOUniqGenes.plot(x='HGNC', y='NoOfVarsMutsCasesKnownInNSCLCPopForThisGene', kind='bar', color="C2") 
plt.savefig('./OutputSummaryPlots/Summary3_drug-gene-variants-effects-GO_NSCLC_Patient4.png')
summaryGOUniqGenes.plot(x='HGNC', y='NoOfGOtermsKnownToBeSignificantInNSCLCforThisGene', kind='bar', color="C3") 
plt.savefig('./OutputSummaryPlots/Summary4_drug-gene-variants-effects-GO_NSCLC_Patient4.png')
#plt.show()
#ax = summaryVarUniqGenes.plot.bar(rot=0)


# In[106]:


#lets also look at the number of drug-gene pairs obtained by Filtering By Overlap with ClinVar
#note this simple summary does not work by including 'relation' column
FilterByOvClinVarUniqDrugGenes=summary1[['CHEBI', 'HGNC']]
FilterByOvClinVarUniqDrugGenes=summary1.groupby(['CHEBI', 'HGNC']).size()
#convert series to dataframe
FilterByOvClinVarUniqDrugGenes=FilterByOvClinVarUniqDrugGenes.to_frame().reset_index()
#name columns
FilterByOvClinVarUniqDrugGenes.columns=('CHEBI','HGNC', 'ClinVar_counts_ignore_column')
#save this to excel
#FilterByOvClinVarUniqDrugGenes


# In[107]:


#lets also look at the number of drug-gene pairs obtained by Filtering By Overlap with GO
#note this simple summary does not work by including 'relation' column
FilterByOvGOUniqDrugGenes=summary2[['CHEBI', 'HGNC']]
FilterByOvGOUniqDrugGenes=summary2.groupby(['CHEBI', 'HGNC']).size()
#convert series to dataframe
FilterByOvGOUniqDrugGenes=FilterByOvGOUniqDrugGenes.to_frame().reset_index()
#name columns
FilterByOvGOUniqDrugGenes.columns=('CHEBI','HGNC', 'GO_counts_ignore_column')
#save this to excel
#FilterByOvGOUniqDrugGenes


# In[108]:


#we will export this dataframe to a excel data format
with pd.ExcelWriter('./OutputSummaryTables/Summary_drug-gene-variants-effects-GO_NSCLC_Patient4.xlsx') as writer:
    summaryVarUniqGenes.to_excel(writer, sheet_name='ClinVarSummaryCounts_pat4')
    summaryGOUniqGenes.to_excel(writer, sheet_name='GOtermsSummaryCounts_pat4')
    FilterByOvClinVarUniqDrugGenes.to_excel(writer, sheet_name='DrugGenePairFromOvClinVar_pat4')
    FilterByOvGOUniqDrugGenes.to_excel(writer, sheet_name='DrugGenePairFromOvGO_pat4')
    summary1.to_excel(writer, sheet_name='_pat4_FilterByOvClinVar')
    summary2.to_excel(writer, sheet_name='_pat4_FilterByOvGO')


# #### To export full script run report click 'File' tab --> click 'Download as' --> click 'HTML (.html)'. The file will get downloaded in the default download folder
# 
# #### To export full script run report as 'pdf' click 'File' tab --> click 'Download as' --> click 'PDF via LaTex(.pdf)'. The file will get downloaded in the default download folder. Even after installing LaTex the direct download as pdf may not work. So an alternative way is, to download as .html first as describe above and then use online tool like  https://html2pdf.com/ to convert .html to .pdf. 
# 
# #### exit notebook by click 'File' tab --> click 'Close and Halt'
# 
# #### click 'Logout' in home to close jupyter notebook
# 
# #### (optional) close Anaconda Navigator
# 
# # End
