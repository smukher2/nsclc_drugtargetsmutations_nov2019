#!/usr/bin/env python
# coding: utf-8

# # Write a python script to ‘capture’, 'filter' and make 'machine readable' relevant biological knowledge 
# 
# # Code 1: Analysis at population level 
# 
# ### Created 8/4/19; Last updated 9/27/19
# ### Student: Shradha Mukherjee, PhD
# ### External Advisor, Site Preceptor: William Hayes, PhD 
# ### Faculty Project Advisor: Robert Greenes, MD, PhD
# 
# pre-requisite: an internet connection and a laptop is required
# 
# pre-requisite software requirement: For mac or windows or linux computer please install anaconda/python3 from https://www.anaconda.com/distribution/. This is the only free software installation required for this analysis. 
# 
# After installation, start jupyter notebook from anaconda navigator as shown below (screenshots)
# 
# #### Open Jupyter from Anaconda navigator 
# ![alt text](./screenshots/anaconda_navigator_screenshot1.png "image1_anaconda_navigator")
# Wait for 10-20 seconds for Anaconda Navigator to initialize, if even after 20seconds it does not open. Then right click on Anaconda navigator and click 'run as administrator', wait 10-15 seconds it should open, click 'Yes' if prompt asks for permission to open. 
# 
# #### Click on the .ipynb file to start the jupyter notebook
# ![alt text](./screenshots/anaconda_navigator_screenshot2.png "image2_anaconda_navigator")
# 
# #### run the script by clicking on the 'Cell' tab --> then clicking on 'Run All' option
# 
# #### keyboard shortcuts for runnign jupyter notebook 
# https://www.dataquest.io/blog/jupyter-notebook-tips-tricks-shortcuts/

# # Knowledge in BEL is expressed as BEL Statements 
# Subject, is a BEL Term. Just SUBJECT only is an allowed BEL statement. 
# Predicate, is one of the BEL relationship types. Relationship
# Object, is can be a BEL Term or another BEL statement
# A typical BEL statement is of the form, <Subject Predicate Object>
# 
# #screenshot below shows available relations in newest version of BEL2.1.0 reference
# https://language.bel.bio/language/reference/2.1.0/relations/
# ![alt text](./screenshots/BEL_2.1.0_newest_source_relations.png "image1_BEL_2.1.0")

# # Step1A_part1 (manually done): To obtain list of most commonly mutated genes
# 
# Prototype for NSCLC: The most commonly mutated genes in NSCLC were obtained from https://www.mycancergenome.org/content/disease/non-small-cell-lung-carcinoma/ and copied to an excel file manually Step1A_MostCommonMutatedGenes.xlsx
# 
# Generalization for any cancer or disease of interest: 
# •	Similar to the example of NSCLC shown above, a list of most commonly mutated genes for any type of cancer can be obtained by clicking on the cancer disease names on https://www.mycancergenome.org/content/disease/
# •	Additionally, custom list of disease genes for other diseases from other websites or patient mutation profile are also compatible with our pipeline. 
# •	All such gene lists must be copied and saved in an excel file, using template Step1A_MostCommonMutatedGenes.xlsx to proceed to Step1B of pipeline.  
# 
# ![alt text](./screenshots/mycancergenome_screenshot.png "image1_mycancergenome")
# 
# Optional: The exact frequency or number of cases plotted in the histogram can be obtained for each gene by ‘hovering’ with mouse arrow on top of the histogram bars (screenshot below). 

# Alternative for any cancer of interest:
# 
# •	The data on https://www.mycancergenome.org/content/disease/non-small-cell-lung-carcinoma/ is from AACR, which is available for download with registration on cbioportal http://genie.cbioportal.org/study/summary?id=genie_public
# •	After selecting the cancer of interest one can download the mutated genes and frequency of mutation in one step (screenshot below). 
# ![alt text](./screenshots/cbioportal_screenshot1.png "image2_cbioportal")
# ![alt text](./screenshots/cbioportal_screenshot2.png "image2_cbioportal")

# # Step1A_part2 of workflow (manually done): ClinVar Data: To obtain list of most common genes and gene variants with functional consequence in NSCLC.
# 
# (manual option)using query term ‘Non-small cell lung cancer’ Simple ClinVar database was searched to retrieve all known variants for genes with functinal consequence present in ClinVar (screenshot below). Simple ClinVar database is a curated version of ClinVar that was created to make ClinVar readily usable. Simple ClinVar is presently updated on a monthly basis. 
# 
# #### Input query
# ![alt text](./screenshots/SimpleClinVar_image1.png "image1_SimpleClinVar")
# 
# #### View results and click 'Show Table'
# ![alt text](./screenshots/SimpleClinVar_image2.png "image1_SimpleClinVar")
# 
# #### Download xlsx files
# ![alt text](./screenshots/SimpleClinVar_image3.png "image1_SimpleClinVar")
# 
# #### put the downloaded files in the 'InputFromUser' folder where this jupyter notebook is placed
# 

# # Step1A_part3 of workflow (manually done): PCViz Data: To obtain list of genes interacting in pathways for most commonly mutated NSCLC genes (prepared in Step1A_part1).
# 
# (manual option)From BioGRID tools https://wiki.thebiogrid.org/doku.php/tools  page navigated to Pathway Commons which has the PCViz search https://www.pathwaycommons.org/. Using as query term the most commonly mutate genes in NSCLC list, './InputFromUser/Step1A_MostCommonMutatedGenes.xlsx' in PCViz search was done to retrieve all known interaction genes for this list of input genes, both direct and indirect (screenshot below). 
# 
# #### Input query
# ![alt text](./screenshots/PCViz_image1.png "image1_PCViz")
# 
# #### View results 
# ![alt text](./screenshots/PCViz_image2.png "image2_PCViz")
# 
# #### Download network sif files
# Then results were exported as SIF files i.e. tabular text file form, then imported this text file into a excel sheet, and gave the columns names 'gene', 'interaction' and 'target'. Save this for further processing in this jupyter notebook, './InputFromUser/PCViz_large_network_sif.xlsx' and './InputFromUser/PCViz_network_sif.xlsx' (screenshot below). 
# ![alt text](./screenshots/PCViz_image3.png "image3_PCViz")
# 
# #### put the downloaded files in the 'InputFromUser' folder where this jupyter notebook is placed

# In[1]:


#############################################################################################################################


# ## Steps below are automated for disease 'NSCLC' ###################################

# In[2]:


#############################################################################################################################


# In[3]:


import os


# In[4]:


#get current working directory
cwd=os.getcwd()
cwd


# In[5]:


#to change current working direectory uncomment line below
#os.chdir("/path/to_new_directory")


# In[6]:


#create directories to put results
if not os.path.exists('OutputBELformat'):
    os.makedirs('OutputBELformat')
    
if not os.path.exists('OutputDefaultFormat'):
    os.makedirs('OutputDefaultFormat')
    
if not os.path.exists('OutputNanoPubsFormat'):
    os.makedirs('OutputNanoPubsFormat')

if not os.path.exists('OutputSummaryPlots'):
    os.makedirs('OutputSummaryPlots')
    
if not os.path.exists('OutputSummaryTables'):
    os.makedirs('OutputSummaryTables')


# In[7]:


#list all files in the current directory
os.listdir('.')


# In[8]:


import pandas as pd
import numpy as np


# # Step1B of workflow: gProfileR and Enrichr Data: To obtain WikiPathways and other functional characterization terms significantly enriched in most commonly mutated NSCLC genes (prepared in Step1A_part1).
# 
# gProfiler https://biit.cs.ut.ee/gprofiler/gost web tool is searched with the list of most commonly mutated NSCLC genes (obtained in Step1A) to identify most significant wikipathways found in this list. This grpofiler analysis is done here using their API, which is an automated process. (see screenshot folder for more information)

# In[9]:


#assign spreadsheat filename to file
file1='./InputFromUser/Step1A_MostCommonMutatedGenes.xlsx'
#load spreadsheet
x1=pd.ExcelFile(file1)
#print sheet names
print(x1.sheet_names)


# In[10]:


#we have only one sheet we will load this into a dataframe
df1=x1.parse('Sheet1')


# In[11]:


#let's look at the number of rows and columns in the dataframe
df1.shape


# In[12]:


#let's look at the first few lines of the dataframe
df1.head()
#df1.head(3)


# In[13]:


#lets view the headers or colnames in the data frame
list(df1.columns)


# In[14]:


#now we select and keep only the gene names that we need 
df1Genes=df1[['Gene']]
#lets few the first few Genes
df1Genes.head()


# In[15]:


df1GenesList=df1Genes.values.T.tolist()
type(df1GenesList)
df1GenesValues=df1GenesList[0]
df1GenesValues


# In[16]:


#connect to gprofiler API to get wiki pathways
import requests
r = requests.post(
    url='https://biit.cs.ut.ee/gprofiler/api/gost/profile/',
    json={
        'organism':'hsapiens',
        'query':df1GenesValues,
        'sources':["WP"],
    }
    )
r.json()['result']

import json
import csv
import pandas as pd
df_WP = pd.DataFrame(r.json()['result'])

#uncomment line below to export as .csv file format
#df.to_csv("Step1B_WikiPathways_MostCommonMutatedGenes.csv", index=False)
#uncomment line below to export as .excel file format
#df.to_excel("Step1B_WikiPathways_MostCommonMutatedGenes.xlsx", sheet_name='WikiPathways')


# In[17]:


#connect to gprofiler API to get reactome
import requests
r = requests.post(
    url='https://biit.cs.ut.ee/gprofiler/api/gost/profile/',
    json={
        'organism':'hsapiens',
        'query':df1GenesValues,
        'sources':["REAC"],
    }
    )
r.json()['result']

import json
import csv
import pandas as pddf_REAC
df_REAC = pd.DataFrame(r.json()['result'])

#uncomment line below to export as .csv file format
#df.to_csv("Step1B_Reactome_MostCommonMutatedGenes.csv", index=False)


# In[18]:


#connect to gprofiler API to get kegg terms
import requests
r = requests.post(
    url='https://biit.cs.ut.ee/gprofiler/api/gost/profile/',
    json={
        'organism':'hsapiens',
        'query':df1GenesValues,
        'sources':["KEGG"],
    }
    )
r.json()['result']

import json
import csv
import pandas as pd
df_KEGG = pd.DataFrame(r.json()['result'])

#uncomment line below to export as .csv file format
#df.to_csv("Step1B_KEGG_MostCommonMutatedGenes.csv", index=False)


# In[19]:


#connect to gprofiler API to get GO:BP terms
import requests
r = requests.post(
    url='https://biit.cs.ut.ee/gprofiler/api/gost/profile/',
    json={
        'organism':'hsapiens',
        'query':df1GenesValues,
        'sources':["GO:BP"],
    }
    )
r.json()['result']

import json
import csv
import pandas as pd
df_GO_BP = pd.DataFrame(r.json()['result'])

#uncomment line below to export as .csv file format
#df.to_csv("Step1B_GO_BP_MostCommonMutatedGenes.csv", index=False)


# In[20]:


with pd.ExcelWriter('./OutputDefaultFormat/Step1B_gprofiler_results_gene-gene-interactions_NSCLC_Population.xlsx') as writer:
    df_WP.to_excel(writer, sheet_name='WikiPathways')
    df_REAC.to_excel(writer, sheet_name='Reactome')
    df_KEGG.to_excel(writer, sheet_name='KEGG')
    df_GO_BP.to_excel(writer, sheet_name='GO_BP')


# #### Now we will convert Reactome terms to BEL statement components and nanopubs to load to BioDati Studio

# #### The above GO terms, Reactome, KEGG pathway terms do not show the actual genes that overlapped so we will use EnrichR to get that information

# In[21]:


#code modified from EnrichR github page https://github.com/snewhouse/enrichr-api/blob/master/query_enrichr_py3.py

import json
import requests
import sys

#define library, see list of libraries available here https://amp.pharm.mssm.edu/Enrichr/#stats
enrichr_library = "GO_Biological_Process_2018"
listdesrciption = "BMI593_BEL_NSCLC_Fall2019"
enrichr_results = "Step1B_enrichrGO_results_gene-gene-interactions_NSCLC_Population"

## enrichr url
ENRICHR_URL = 'http://amp.pharm.mssm.edu/Enrichr/addList'

# stick gene list here
genes_str='\n'.join(df1GenesValues)

# name of analysis or list
description = str(listdesrciption)

# payload
payload = {
  'list': (None, genes_str),
  'description': (None, description)
}

# response
print("Enrichr API : requests.post")
response = requests.post(ENRICHR_URL, files=payload)

if not response.ok:
  raise Exception('Error analyzing gene list')

job_id = json.loads(response.text)

print('Enrichr API : Job ID:', job_id)

################################################################################
# View added gene list
#
ENRICHR_URL_A = 'http://amp.pharm.mssm.edu/Enrichr/view?userListId=%s'

user_list_id = job_id['userListId']
print(user_list_id)

response_gene_list = requests.get(ENRICHR_URL_A % str(user_list_id))

if not response_gene_list.ok:
    raise Exception('Error getting gene list')

print('Enrichr API : View added gene list:', job_id)
added_gene_list = json.loads(response_gene_list.text)
print(added_gene_list)

################################################################################
# Get enrichment results
#
ENRICHR_URL = 'http://amp.pharm.mssm.edu/Enrichr/enrich'
query_string = '?userListId=%s&backgroundType=%s'

## get id data
user_list_id = job_id['userListId']

## Libraray
gene_set_library = str(enrichr_library)

response = requests.get(
    ENRICHR_URL + query_string % (str(user_list_id), gene_set_library)
 )
if not response.ok:
    raise Exception('Error fetching enrichment results')

print('Enrichr API : Get enrichment results: Job Id:', job_id)
data = json.loads(response.text)
print(data)

################################################################################
## Download file of enrichment results
#
ENRICHR_URL = 'http://amp.pharm.mssm.edu/Enrichr/export'

query_string = '?userListId=%s&filename=%s&backgroundType=%s'

user_list_id = str(job_id['userListId'])

filename = enrichr_results

gene_set_library = str(enrichr_library)

url = ENRICHR_URL + query_string % (user_list_id, filename, gene_set_library)

response = requests.get(url, stream=True)

print('Enrichr API : Downloading file of enrichment results: Job Id:', job_id)
with open('./OutputDefaultFormat/' + filename + '.txt', 'wb') as f:
    for chunk in response.iter_content(chunk_size=1024):
        if chunk:
            f.write(chunk)
################################################
print('Enrichr API : Results written to:', enrichr_results + ".txt")
print("Enrichr API : Done")


# In[22]:


#now we import the txt file of results and filter to keep only significant GO Biological processes
#f = open('./OutputDefaultFormat/NSCLC_enrichment_GO_Biological_Process_2018.txt', 'r')
#df_enrichR = f.read()
import pandas as pd
df_enrichrGO=pd.read_csv('./OutputDefaultFormat/Step1B_enrichrGO_results_gene-gene-interactions_NSCLC_Population.txt', sep='\t')


# #### Now we will convert df_enrichR to BEL statement components and nanopubs to load to BioDati Studio

# In[23]:


#lets view the changed dataframe
df_enrichrGO.head()


# In[24]:


#now we filter the dataframe to keep only significant p-value terms
df_enrichrGO=df_enrichrGO[df_enrichrGO['Adjusted P-value'] < 0.05]
df_enrichrGO.head()


# In[25]:


#reference for split code https://stackoverflow.com/questions/12680754/split-explode-pandas-dataframe-string-entry-to-separate-rows
df_enrichrGO_Genes1=pd.concat([pd.Series(row['Term'], row['Genes'].split(';'))              
                    for _, row in df_enrichrGO.iterrows()]).reset_index()
df_enrichrGO_Genes1.head()


# In[26]:


#let's look at the number of rows and columns in the dataframe
#notice we have more rows now because we split each gene symbol into seperate rows
df_enrichrGO.shape, df_enrichrGO_Genes1.shape


# In[27]:


#change data.columns=[headerName] into data.columns=headerName if get error 'dataframe' object has no attribute 'str'
#https://stackoverflow.com/questions/51502263/pandas-dataframe-object-has-no-attribute-str
#now we name columns
df_enrichrGO_Genes1.columns='Genes', 'Term'
df_enrichrGO_Genes1.head()


# In[28]:


#now we further split to seperate out the GO terms
df_enrichrGO_Genes2=df_enrichrGO_Genes1
df_enrichrGO_Genes2[["One", "Two"]]= df_enrichrGO_Genes1.Term.str.split('(', expand = True)
df_enrichrGO_Genes3=df_enrichrGO_Genes2
df_enrichrGO_Genes3[["Three", "Four"]]= df_enrichrGO_Genes2.Two.str.split(')', expand = True)
df_enrichrGO_Genes3.head()


# In[29]:


#now we select and keep only those columns that we need 
#to keep GO id uncomment line below
#df_enrichrGO_Genessel=df_enrichrGO_Genes3[['Genes', 'Three']]
#to keep GO term uncomment line below
df_enrichrGO_Genessel=df_enrichrGO_Genes3[['Genes', 'One']]
#rename column 
df_enrichrGO_Genessel.columns='Genes', 'Term'
df_enrichrGO_Genessel.head()


# In[30]:


#here we add some more columns that will be needed for BEL statements
#the source is GO website
df_enrichrGO_Genessel['citations']='http://geneontology.org/docs/go-enrichment-analysis/'
#the database used was enrichR
df_enrichrGO_Genessel['database_url']='https://amp.pharm.mssm.edu/Enrichr/'
#the relation is 'increases' since the presence of the gene increases the likelihood of GO term
df_enrichrGO_Genessel['interaction']='increases'


# In[31]:


#now we rename columns giving them specific names that are available on BEL namespace.
df_enrichrGO_Genessel=df_enrichrGO_Genessel.rename(columns={'Genes':'HGNC','interaction':'relation','Term':'GO', 'citations':'citations','database_url':'database_url'})

#lets few the first few rows
df_enrichrGO_Genessel.head()


# In[32]:


#in the GO column has multiple GO terms
#so lets first look at the different types of values present in the GO column
df_enrichrGO_Genessel.GO.unique()


# In[33]:


#we will export this dataframe to a excel data format
df_enrichrGO_Genessel.to_excel("./OutputBELformat/Step1B_enrichrGO_results_forBEL_gene-gene-interactions_NSCLC_Population.xlsx", sheet_name='enrichrGO_BEL')

#uncomment line below to export as csv file
#Step1C_csv=df_DGIdbsel.to_csv("Step1B_enrichrGO_results_forBEL_gene-gene-interactions_NSCLC_Population.csv", index=False)


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[34]:


df_enrichrGO_Genesselnp=df_enrichrGO_Genessel


# In[35]:


#now we rename columns giving them specific names that are available on BEL namespace
df_enrichrGO_Genesselnp=df_enrichrGO_Genesselnp.rename(columns={'HGNC':'subject', 'relation':'relation',  'GO':'obj', 'citations':'citations', 'database_url':'db_url'})
#change column order
df_enrichrGO_Genesselnp=df_enrichrGO_Genesselnp[['subject', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_enrichrGO_Genesselnp.head()


# In[36]:


#Export results as csv to use in next step
df_enrichrGO_Genesselnp.to_csv("./OutputNanoPubsFormat/Step1B_enrichrGO_results_forBELnp_gene-gene-interactions_NSCLC_Population.csv")


# In[37]:


#William Hayes function modified for making enrichR GO gene-gene-effect nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pop_v1"

#define function
def process_step1b_file():
    source = "https://amp.pharm.mssm.edu/Enrichr/"
    fn = "./OutputNanoPubsFormat/Step1B_enrichrGO_results_forBELnp_gene-gene-interactions_NSCLC_Population.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, relation, obj, citation, db_url) = row
            nanopub = {}
            subj = f"p(HGNC:{subject})"
            obj = f"bp(GO:{obj})"         


            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_enrichR_GO_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": citation},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "database":{"uri_db": db_url},
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pop_v1",
                            "gd_creator": creator,
                            "note": f"{subject} acts as {relation} of {obj}. This relation may or may not hold true in the context of NSCLC.",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1b_file()

    with open("./OutputNanoPubsFormat/Step1B_enrichrGO_results_forBELnp_gene-gene-interactions_NSCLC_Population.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # Step1C of workflow: DGIdb Data: To obtain possible drugs to target the list of most commonly mutated NSCLC genes (prepared in Step1A_part1).
# DGIdb http://www.dgidb.org/search_interactions web tool can be searched with list of most commonly mutated NSCLC genes to identify possible FDA approved drugs that can target the list. This DGIdb analysis is done using their API http://www.dgidb.org/api, which is an automated process. (Also see screenshot folder for more information)

# In[38]:


print(df1GenesValues)


# In[39]:


str1 = ','.join(df1GenesValues)
str1
#url=print("http://dgidb.org/api/v2/interactions.json?genes=" + str1)
url=''.join("http://dgidb.org/api/v2/interactions.json?genes=" + str1)
url


# In[40]:


#print(df1GenesValues)
# importing the requests library 
import requests 

# sending get request and saving the response as response object 
#r = requests.get("url") #does not work
  
r = requests.get(''.join("http://dgidb.org/api/v2/interactions.json?genes=" + str1)) 
  
# extracting data in json format 
r.json() 

#extracting keys
r.json().keys()


# In[41]:


import json 
import pandas as pd 
from pandas.io.json import json_normalize #package for flattening json in pandas df

r_norm = json_normalize(r.json()['matchedTerms'])

#lets few the first few rows
r_norm.head()


# In[42]:


import json 
import pandas as pd 
from pandas.io.json import json_normalize #package for flattening json in pandas df

#r_norm = json_normalize(r.json()['matchedTerms'])
df_DGIdb = json_normalize(r.json()['matchedTerms'], 'interactions', meta='geneName')

#lets few the first few rows
df_DGIdb.head()


# In[43]:


#remove square bracket around values
df_DGIdb['interactionTypes']= df_DGIdb['interactionTypes'].str.get(0)
df_DGIdb['pmids']= df_DGIdb['pmids'].str.get(0)
df_DGIdb['sources']= df_DGIdb['sources'].str.get(0)

#lets few the first few rows
df_DGIdb.head()


# In[44]:


#From interaction id create url for the interaction
df_DGIdb['interactionId_url']=df_DGIdb['interactionId']
df_DGIdb['interactionId_url']= 'http://www.dgidb.org/interactions/' + df_DGIdb['interactionId_url'].astype(str) + '#_summary'

#From interaction id create url for the interaction
df_DGIdb['pmids_one_url']=df_DGIdb['pmids']
df_DGIdb['pmids_one_url']= 'https://www.ncbi.nlm.nih.gov/pubmed/' + df_DGIdb['pmids_one_url'].astype(str)

#lets few the first few rows
df_DGIdb.head()


# In[45]:


#From interaction id create url for the interaction
df_DGIdb['pmids_one_url']=df_DGIdb['pmids']
df_DGIdb['pmids_one_url']= 'https://www.ncbi.nlm.nih.gov/pubmed/' + df_DGIdb['pmids_one_url'].astype(str)

#lets few the first few rows
df_DGIdb.head()


# In[46]:


#in the 'interactionType' column some values have multiple synonyms, which we want to replace with a single BEL v 2.1.0 availabel relation term
#so lets first look at the different types of values present in the interaction_types column
df_DGIdb.interactionTypes.unique()


# In[47]:


#we will export this dataframe to a excel data format
df_DGIdb.to_excel("./OutputDefaultFormat/Step1C_DGIdb_results_drug-gene-interactions_NSCLC_Population.xlsx", sheet_name='DGIdb')

#uncomment line below to export as csv file
#df_DGIdb_csv=r_norm.to_csv("Step1C_DGIdb_results_drug-gene-interactions_NSCLC.csv", index=False)


# #### Now we will convert df_DGIdb to BEL statement components and nanopubs to load to BioDati Studio

# In[48]:


#let's look at the number of rows and columns in the dataframe
df_DGIdb.shape


# In[49]:


#let's look at the first few lines of the dataframe
df_DGIdb.head(3)


# In[50]:


#in the 'interactionType' column some values have multiple synonyms, which we want to replace with a single BEL v 2.1.0 availabel relation term
#so lets first look at the different types of values present in the interaction_types column
df_DGIdb.interactionTypes.unique()


# In[51]:


#Using available BEL2.1.0 'predicate' or relationship options we will replace 
#the values of interactionTypes to valid BEL relationship 
#you can view the relationships here: https://language.bel.bio/language/reference/2.1.0/relations/ 
#create a dictionary
x_remap = {'agonist': 'increases', 
        'positive allosteric modulator': 'increases',  
        'activator': 'increases', 
        'agonist,allosteric modulator': 'increases', 
        'stimulator': 'increases', 
        'inducer': 'increases' }

y_remap = {'inhibitor':'decreases',
        'antagonist':'decreases',
        'blocker':'decreases',
        'channel blocker':'decreases',
        'antisense':'decreases',
        'inverse agonist':'decreases',
        'gating inhibitor':'decreases',
        'negative modulator':'decreases',
        'antisense oligonucleotide':'decreases',
        'allosteric modulator,antagonist':'decreases',
        'channel blocker,gating inhibitor':'decreases',
        'antagonist,inhibitor':'decreases',
        'inhibitory allosteric modulator':'decreases',
        'suppressor':'decreases'}
    
z_remap = {'binder': 'association', 
        'modulator': 'regulates', 
        'antibody': 'association', 
        'allosteric modulator': 'regulates', 
        'vaccine': 'association', 
        'activator,antagonist': 'regulates', 
        'cofactor': 'association', 
        'activator,channel blocker': 'regulates', 
        'partial agonist': 'regulates', 
        'agonist,antagonist': 'regulates'}


# In[52]:


df_DGIdb.replace({"interactionTypes": x_remap})
df_DGIdb.replace({"interactionTypes": y_remap})
df_DGIdb.replace({"interactionTypes": z_remap})

#replace 'NaN' or unspecified interactionType value with 'association'
df_DGIdb['interactionTypes'].fillna('association', inplace=True)

#lets few the first few rows
df_DGIdb.head()


# In[53]:


#lets view the headers or colnames in the data frame
list(df_DGIdb.columns)


# In[54]:


#now we select and keep only those columns that we need 
#df2sel=df2[['gene_name','drug_name','interaction_types','PMIDs']]
df_DGIdbsel=df_DGIdb[['drugChemblId','interactionTypes','geneName', 'pmids_one_url','interactionId_url', 'drugName', 'score']]
#'drugName' and score' are saved to prepare summary file not for BEL

#alternative 2
#df2.loc[:,'gene':'pmids']
df_DGIdbsel.head()


# In[55]:


#now we rename columns giving them specific names that are available on BEL namespace.
df_DGIdbsel=df_DGIdbsel.rename(columns={'drugChemblId':'CHEBI','interactionTypes':'relation','geneName':'HGNC', 'pmids_one_url':'citations','interactionId_url':'database_url','drugName':'drugName', 'score':'score'})

#lets few the first few rows
df_DGIdbsel.head()


# In[56]:


#in the 'interaction_type' column some values have multiple synonyms, which we want to replace with a single BEL v 2.1.0 availabel relation term
#so lets first look at the different types of values present in the interaction_types column
df_DGIdbsel.relation.unique()


# In[57]:


#we will export this dataframe to a excel data format
df_DGIdbsel.to_excel("./OutputBELformat/Step1C_DGIdb_results_forBEL_drug-gene-interactions_NSCLC_Population.xlsx", sheet_name='DGIdb_BEL')

#uncomment line below to export as csv file
#Step1C_csv=df_DGIdbsel.to_csv("Step1C_DGIdb_results_ForBEL_drug-gene-interactions_NSCLC.csv", index=False)


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[58]:


df_DGIdbselnp=df_DGIdbsel
#database url is same as db_url, will be renamed in the next step


# In[59]:


#now we rename columns giving them specific names that are available on BEL namespace
df_DGIdbselnp=df_DGIdbselnp.rename(columns={'CHEBI':'subject', 'relation':'relation',  'HGNC':'obj', 'citations':'citations', 'database_url':'db_url'})
#change column order adn do not use 'drugName' and 'score' which are not for BEL
df_DGIdbselnp=df_DGIdbselnp[['subject', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_DGIdbselnp.head()


# In[60]:


#Export results as csv to use in next step
df_DGIdbselnp.to_csv("./OutputNanoPubsFormat/Step1C_DGIdb_results_forBELnp_drug-gene-interactions_NSCLC_Population.csv")


# In[61]:


#William Hayes function modified for making drug-gene-effect nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pop_v1"

#define function
def process_step1d_file():
    source = "http://www.dgidb.org/"
    fn = "./OutputNanoPubsFormat/Step1C_DGIdb_results_forBELnp_drug-gene-interactions_NSCLC_Population.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, relation, obj, citation, db_url) = row
            nanopub = {}
            subj = f"a(CHEBI:{subject})"
            obj = f"p(HGNC:{obj})"         


            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_DGIdb_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": citation},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "database":{"uri_db": db_url},
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pop_v1",
                            "gd_creator": creator,
                            "note": f"{subject} acts as {relation} of {obj}. This relation may or may not hold true in the context of NSCLC.",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1d_file()

    with open("./OutputNanoPubsFormat/Step1C_DGIdb_results_forBELnp_drug-gene-interactions_NSCLC_Population.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # Step1D of workflow continued (automated): ClinVar Data: To compare list of most common variants with functional consequence with list of most commonly mutated NSCLC genes (prepared in Step1A_part1).

# In[62]:


#assign spreadsheat filename to file
file3='./InputFromUser/ShinyClinVar_SNV.xlsx'
#load spreadsheet
x3=pd.ExcelFile(file3)
#print sheet names
print(x3.sheet_names)


# In[63]:


#we have only one sheet we will load this into a dataframe
df3SNV=x3.parse('Sheet1')


# In[64]:


#assign spreadsheat filename to file
file3='./InputFromUser/ShinyClinVar_CNV.xlsx'
#load spreadsheet
x3=pd.ExcelFile(file3)
#print sheet names
print(x3.sheet_names)


# In[65]:


#we have only one sheet we will load this into a dataframe
df3CNV=x3.parse('Sheet1')


# In[66]:


#now we concatenate or join the dataframes vertically
all_df3=[df3SNV, df3CNV]
df_ClinVar=pd.concat(all_df3).reset_index(drop=True)


# In[67]:


#let's look at the number of rows and columns in the dataframe
print(df3SNV.shape,df3CNV.shape,df_ClinVar.shape)
#note that the df3 columns are a sum of the first two dataframes


# In[68]:


#lets view the changed dataframe
df_ClinVar.head()


# In[69]:


#lets view the headers or colnames in the data frame
list(df_ClinVar.columns)


# In[70]:


#now lets filter the gene_names with NSCLC genes
df1GenesValues
df_ClinVarNew=df_ClinVar[df_ClinVar.GeneSymbol.isin(df1GenesValues)]

#notice after filtering we have fewer entries
print(df_ClinVar.shape, df_ClinVarNew.shape)


# In[71]:


#in the 'ClinicalSignificance' column there are different values, but we want to keep only 'pathogenic', and replace with BEL v 2.1.0 availabel relation term
#so lets first look at the different types of values present in the ClinicalSignificance column
df_ClinVarNew.ClinicalSignificance.unique()


# In[72]:


#we will export this dataframe to a excel data format
df_ClinVarNew.to_excel("./OutputDefaultFormat/Step1D_ClinVar_results_gene-variants-effects_NSCLC_Population.xlsx", sheet_name='ClinVar')

#uncomment line below to export as csv file
#df_ClinVarNew_csv=df_ClinVarNew.to_csv("Step1D_ClinVar_results_gene-variants-effects_NSCLC.csv", index=False)


# #### Now we will convert df_ClinVarNew to BEL statement components and nanopubs to load to BioDati Studio

# In[73]:


#now we select and keep only those columns that we need 
#df2sel=df2[['GeneSymbol','ClinicalSignificance','Name','Link']]
df_ClinVarsel=df_ClinVarNew[['GeneSymbol','ClinicalSignificance','Name','Link']]

#alternative 2
#df2.loc[:,'gene':'pmids']
df_ClinVarsel.head()


# In[74]:


#filter dataframe to keep only 'Pathogenic'
df_ClinVarsel_1 = df_ClinVarsel[df_ClinVarsel['ClinicalSignificance']=='Pathogenic']
#notice after filtering we have fewer entries
print(df_ClinVarsel.shape, df_ClinVarsel_1.shape)


# In[75]:


#Pathogenic is not a valid BEL relationship (you can view the relationships here: https://language.bel.bio/language/reference/2.1.0/relations/ 
#The relation in the nanopubs should probably be 'increases', assuming increases in that gene variant increases lung cancer - there could also be decreases or unknown. 
#If unknown, then it's either 'association' or more informative and preferred:  positiveCorrelation or negativeCorrelation.

#so here we replace value 'Pathogenic' with 'increases'
df_ClinVarsel_1['ClinicalSignificance'].replace(['Pathogenic'],['increases'],inplace=True) 


# In[76]:


#reassign to original variable name
df_ClinVarsel=df_ClinVarsel_1
#lets view the new dataframe
df_ClinVarsel.head()


# In[77]:


print(df_ClinVarsel.shape)


# In[78]:


#lets view the headers or colnames in the data frame
list(df_ClinVarsel.columns)


# In[79]:


#diseases can be defines by MESH namespace 'MESH:"Carcinoma, Non-Small-Cell Lung"'
df_ClinVarsel['Disease']='MESH:"Carcinoma, Non-Small-Cell Lung"'
#now we rename columns giving them specific names that are available on BEL namespace
df_ClinVarsel=df_ClinVarsel.rename(columns={'GeneSymbol':'HGNC', 'Name':'Variant', 'ClinicalSignificance':'relation', 'Disease':'Disease', 'Link':'citations'})
#change column order
df_ClinVarsel=df_ClinVarsel[['HGNC', 'Variant', 'relation', 'Disease', 'citations']]
#lets view the new dataframe
df_ClinVarsel.head()


# In[80]:


#uncomment line below to export results as csv file 
#df_ClinVarsel_csv=df_ClinVarsel_BioDati_ready.to_csv("Step1D_ClinVar_results_forBEL_gene-variants-effects_NSCLC.csv", index=False)

#we will export this dataframe to a excel data format
df_ClinVarsel.to_excel("./OutputBELformat/Step1D_ClinVar_results_forBEL_gene-variants-effects_NSCLC_Population.xlsx", sheet_name='ClinVarBEL')


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[81]:


df_ClinVarselnp=df_ClinVarsel
#citation url will also be used as database url as its the same for these entries
df_ClinVarselnp['db_url']=df_ClinVarselnp['citations']


# In[82]:


#now we rename columns giving them specific names that are available on BEL namespace
df_ClinVarselnp=df_ClinVarselnp.rename(columns={'HGNC':'subject', 'Variant':'variant', 'relation':'relation',  'Disease':'obj', 'citations':'citations', 'db_url':'db_url'})
#change column order
df_ClinVarselnp=df_ClinVarselnp[['subject', 'variant', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_ClinVarselnp.head()


# In[83]:


#Export results as csv to use in next step
df_ClinVarselnp.to_csv("./OutputNanoPubsFormat/Step1D_ClinVar_results_forBELnp_gene-variants-effects_NSCLC_Population.csv")


# In[84]:


#William Hayes function for making gene-variant-effects nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pop_v1"

#define function
def process_step1d_file():
    source = "https://www.ncbi.nlm.nih.gov/clinvar/"
    fn = "./OutputNanoPubsFormat/Step1D_ClinVar_results_forBELnp_gene-variants-effects_NSCLC_Population.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, variant, relation, obj, citation, db_url) = row
            nanopub = {}
            matches = re.search(r"^([\w\_\.]+)\(.*\(([\w\.]+)\)", variant)
            if matches:
                subj = f"p(REFSEQ:{matches.group(1)}, var({matches.group(2)}))"
            else:
                subj = f"BADMATCH - {variant}"

            obj = f"path({obj})"

            matches = re.match(r".*?\"(.*?)\"", citation)
            if matches:
                uri = matches.group(1)
            else:
                uri = f"BADMATCH - {citation}"

            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_clinvar_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": uri},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pop_v1",
                            "gd_creator": creator,
                            "note": f"{subject} mutation increases non-small cell lung cancer",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1d_file()

    with open("./OutputNanoPubsFormat/Step1D_ClinVar_results_forBELnp_gene-variants-effects_NSCLC_Population.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # Step1E of workflow continued (automated): PCViz Data: To compare list of genes interacting in pathways to the most commonly mutated NSCLC genes (prepared in Step1A_part1).

# In[85]:


#assign spreadsheat filename to file
file4='./InputFromUser/PCViz_large_network_sif.xlsx'
#load spreadsheet
x4=pd.ExcelFile(file4)
#print sheet names
print(x4.sheet_names)


# In[86]:


#we have only one sheet we will load this into a dataframe
df_PCViz=x4.parse('Sheet1')


# In[87]:


#let's look at the number of rows and columns in the dataframe
print(df_PCViz.shape)


# In[88]:


#lets view the changed dataframe
df_PCViz.head()


# In[89]:


#lets view the headers or colnames in the data frame
list(df_PCViz.columns)


# In[90]:


#here we keep only direct interactions between the NSCLC genes
#now lets filter the gene_names with NSCLC genes
df1GenesValues
df_PCVizNew=df_PCViz[df_PCViz.gene.isin(df1GenesValues)]

#now lets filter the target names also with NSCLC genes
df1GenesValues
df_PCVizNew=df_PCViz[df_PCViz.target.isin(df1GenesValues)]

#notice after filtering we have fewer entries
print(df_PCViz.shape, df_PCVizNew.shape)


# In[91]:


#in the 'interaction' column there are different types of interactions, but we want to replace with BEL v 2.1.0 availabel relation terms
#so lets first look at the different types of values present in the ClinicalSignificance column
df_PCVizNew.interaction.unique()


# In[92]:


#we will export this dataframe to a excel data format
df_PCVizNew.to_excel("./OutputDefaultFormat/Step1E_PCViz_results_gene-gene-interactions_NSCLC_Population.xlsx", sheet_name='PCViz')

#uncomment line below to export as csv file
#df_PCVizNew_csv=df_PCVizNew.to_csv("Step1E_PCViz_results_gene-gene-interaction_NSCLC.csv", index=False)


# #### Now we will convert df_PCVizNew to BEL statement components and nanopubs to load to BioDati Studio

# In[93]:


#now we select and keep only those columns that we need, here we need all the original columns 
#df2sel=df2[['gene', 'interaction', 'target']]
df_PCVizsel=df_PCVizNew[['gene', 'interaction', 'target']]

#alternative 2
#df2.loc[:,'gene':'pmids']
df_PCVizsel.head()


# In[94]:


#in the 'interaction' column there are different types of interactions, but we want to replace with BEL v 2.1.0 availabel relation terms
#so lets first look at the different types of values present in the ClinicalSignificance column
df_PCVizsel.interaction.unique()


# In[95]:


#Using available BEL2.1.0 'predicate' or relationship options we will replace 
#the values of interaction types to valid BEL relationship 
#you can view the relationships here: https://language.bel.bio/language/reference/2.1.0/relations/ 

df_PCVizsel.replace({'in-complex-with': 'hasComponent', 
        'controls-state-change-of': 'regulates',  
        'controls-expression-of': 'regulates', 
        'controls-phosphorylation-of': 'regulates', 
        'controls-transport-of': 'regulates', 
        'chemical-affects': 'regulates' })
df_PCVizsel.head()


# In[96]:


print(df_PCVizsel.shape)


# In[97]:


#lets view the headers or colnames in the data frame
list(df_PCVizsel.columns)


# In[98]:


#the PCViz does not provide citation for each interaction, so we will simply put the pubmed link here. 
#User is encouraged to add specific citations from pubmed to the exported file manually later
df_PCVizsel['Search_Link']='https://www.ncbi.nlm.nih.gov/pubmed'

#now we rename columns giving them specific names that are available on BEL namespace
df_PCVizsel=df_PCVizsel.rename(columns={'gene':'HGNC_1', 'interaction':'relation', 'target':'HGNC_2', 'Search_Link':'citations'})
#change column order
#df_PCVizsel=df_PCVizsel[['HGNC', 'relation', 'HGNC', 'citations']]

#lets view the new dataframe
df_PCVizsel.head()


# In[99]:


#uncomment line below to export results as csv file 
#df_PCVizsel_csv=df_PCVizsel_BioDati_ready.to_csv("Step1E_PCViz_results_forBEL_gene-gene-interaction_NSCLC.csv", index=False)

#we will export this dataframe to a excel data format
df_PCVizsel.to_excel("./OutputBELformat/Step1E_PCViz_results_forBEL_gene-gene-interactions_NSCLC_Population.xlsx", sheet_name='PCVizBEL')


# #### Now we create nanopubs in json version that can directly be loaded to BioDati Studio

# In[100]:


df_PCVizselnp=df_PCVizsel
#citation url will also be used as database url as its the same for these entries
df_PCVizselnp['db_url']=df_PCVizselnp['citations']


# In[101]:


#now we rename columns giving them specific names that are available on BEL namespace
df_PCVizselnp=df_PCVizselnp.rename(columns={'HGNC_1':'subject', 'relation':'relation',  'HGNC_2':'obj', 'citations':'citations', 'db_url':'db_url'})
#change column order
df_PCVizselnp=df_PCVizselnp[['subject', 'relation', 'obj', 'citations', 'db_url']]
#lets view the new dataframe
df_PCVizselnp.head()


# In[102]:


#Export results as csv to use in next step
df_PCVizselnp.to_csv("./OutputNanoPubsFormat/Step1E_PCViz_results_forBELnp_gene-gene-interactions_NSCLC_Population.csv")


# In[103]:


#William Hayes function modified for making gene-gene-interaction nano pubs json from dataframe or csv file

#import python libraries
import csv
import json
import re

#defien creator
creator = "shradha_pop_v1"

#define function
def process_step1e_file():
    source = "https://www.pathwaycommons.org/pcviz/"
    fn = "./OutputNanoPubsFormat/Step1E_PCViz_results_forBELnp_gene-gene-interactions_NSCLC_Population.csv"

    nanopubs = []
    with open(fn, "r") as f:
        reader = csv.reader(f)

        i = 0
        first_row = next(reader)

        for row in reader:
            i += 1
            (id, subject, relation, obj, citation, db_url) = row
            nanopub = {}
            subj = f"p(HGNC:{subject})"
            obj = f"p(HGNC:{obj})"         


            nanopubs.append(
                {
                    "nanopub": {
                        "id": f"{creator}_PCViz_{i}",
                        "type": {"name": "BEL", "version": "2.1.0"},
                        "citation": {"uri": citation},
                        "assertions": [{"subject": subj, "relation": relation, "object": obj}],
                        "annotations": [{"type": "Species", "id": "TAX:9606", "label": "human"}],
                        "database":{"uri_db": db_url},
                        "metadata": {
                            "project": "BMI593_BEL_NSCLC_Fall2019_pop_v1",
                            "gd_creator": creator,
                            "note": f"{subject} acts as {relation} of {obj}. This relation may or may not hold true in the context of NSCLC.",
                        },
                    }
                }
            )

    return nanopubs


def main():
    nanopubs = process_step1e_file()

    with open("./OutputNanoPubsFormat/Step1E_PCViz_results_forBELnp_gene-gene-interactions_NSCLC_Population.json", "w") as f:
        json.dump(nanopubs, f, indent=4)


if __name__ == "__main__":
    main()


# # (automated step) Creating a summary table of results: Gene symbol, common variant, drug to use and GO

# In[104]:


summary1 = pd.merge(df_DGIdbsel[['CHEBI', 'relation', 'HGNC', 'citations', 'database_url','drugName', 'score']],
                 df_ClinVarsel[['HGNC', 'relation', 'Variant', 'citations', 'Disease']],
                 on='HGNC')
summary1.head()


# In[105]:


#lets view the headers or colnames in the data frame
list(summary1.columns)
#save this to excel
#summary1


# In[106]:


#This second merge gives too many rows so don't use citation and database url for GO
summary2 = pd.merge(df_DGIdbsel[['CHEBI', 'relation', 'HGNC', 'citations', 'database_url','drugName', 'score']],
                 df_enrichrGO_Genessel[['HGNC', 'relation', 'GO']],
                 on='HGNC')
summary2.head()


# In[107]:


#lets view the headers or colnames in the data frame
list(summary2.columns)
#save this to excel
#summary2


# In[108]:


#lets look at the number of known pathogenic variants in these Pop NSCLC genes 
FilterByOvClinVarUniqGenes=df_ClinVarsel['HGNC'].value_counts()
#convert series to dataframe
FilterByOvClinVarUniqGenes=FilterByOvClinVarUniqGenes.to_frame().reset_index()
#name columns
FilterByOvClinVarUniqGenes.columns=('HGNC','NoOfVarsKnownToBePathogenicInNSCLCforThisGene')
#prepare Pop genes counts
df1_Pop=df1
df1_Pop.columns=('HGNC','NoOfVarsMutsCasesKnownInNSCLCPopForThisGene')
#merge
summaryVarUniqGenes= pd.merge(df1_Pop, FilterByOvClinVarUniqGenes, on='HGNC')
#save this to excel
#summaryVarUniqGenes


# In[109]:


#lets look at the number of GO terms in these Pop NSCLC genes
FilterByOvGOUniqGenes=df_enrichrGO_Genessel['HGNC'].value_counts()
#convert series to dataframe
FilterByOvGOUniqGenes=FilterByOvGOUniqGenes.to_frame().reset_index()
#name columns
FilterByOvGOUniqGenes.columns=('HGNC','NoOfGOtermsKnownToBeSignificantInNSCLCforThisGene')
#prepare Pop genes counts
df1_Pop=df1
df1_Pop.columns=('HGNC','NoOfVarsMutsCasesKnownInNSCLCPopForThisGene')
#merge 
summaryGOUniqGenes= pd.merge(df1_Pop, FilterByOvGOUniqGenes, on='HGNC')
#save this to excel
#summaryGOUniqGenes


# In[110]:


#view and save plots
import matplotlib.pyplot as plt

#pp = PdfPages('Summary_drug-gene-variants-effects-GO_NSCLC_Population.pdf')
summaryVarUniqGenes.plot(x='HGNC', y='NoOfVarsMutsCasesKnownInNSCLCPopForThisGene', kind='bar', color="C2") 
plt.savefig('./OutputSummaryPlots/Summary1_drug-gene-variants-effects-GO_NSCLC_Population.png')
summaryVarUniqGenes.plot(x='HGNC', y='NoOfVarsKnownToBePathogenicInNSCLCforThisGene', kind='bar', color="C3")
plt.savefig('./OutputSummaryPlots/Summary2_drug-gene-variants-effects-GO_NSCLC_Population.png')
summaryGOUniqGenes.plot(x='HGNC', y='NoOfVarsMutsCasesKnownInNSCLCPopForThisGene', kind='bar', color="C2") 
plt.savefig('./OutputSummaryPlots/Summary3_drug-gene-variants-effects-GO_NSCLC_Population.png')
summaryGOUniqGenes.plot(x='HGNC', y='NoOfGOtermsKnownToBeSignificantInNSCLCforThisGene', kind='bar', color="C3") 
plt.savefig('./OutputSummaryPlots/Summary4_drug-gene-variants-effects-GO_NSCLC_Population.png')
#plt.show()
#ax = summaryVarUniqGenes.plot.bar(rot=0)


# In[111]:


#lets also look at the number of drug-gene pairs obtained by Filtering By Overlap with ClinVar
#note this simple summary does not work by including 'relation' column
FilterByOvClinVarUniqDrugGenes=summary1[['CHEBI', 'HGNC']]
FilterByOvClinVarUniqDrugGenes=summary1.groupby(['CHEBI', 'HGNC']).size()
#convert series to dataframe
FilterByOvClinVarUniqDrugGenes=FilterByOvClinVarUniqDrugGenes.to_frame().reset_index()
#name columns
FilterByOvClinVarUniqDrugGenes.columns=('CHEBI','HGNC', 'ClinVar_counts_ignore_column')
#save this to excel
#FilterByOvClinVarUniqDrugGenes


# In[112]:


#lets also look at the number of drug-gene pairs obtained by Filtering By Overlap with GO
#note this simple summary does not work by including 'relation' column
FilterByOvGOUniqDrugGenes=summary2[['CHEBI', 'HGNC']]
FilterByOvGOUniqDrugGenes=summary2.groupby(['CHEBI', 'HGNC']).size()
#convert series to dataframe
FilterByOvGOUniqDrugGenes=FilterByOvGOUniqDrugGenes.to_frame().reset_index()
#name columns
FilterByOvGOUniqDrugGenes.columns=('CHEBI','HGNC', 'GO_counts_ignore_column')
#save this to excel
#FilterByOvGOUniqDrugGenes


# In[113]:


#we will export this dataframe to a excel data format
with pd.ExcelWriter('./OutputSummaryTables/Summary_drug-gene-variants-effects-GO_NSCLC_Population.xlsx') as writer:
    summaryVarUniqGenes.to_excel(writer, sheet_name='ClinVarSummaryCounts_Pop')
    summaryGOUniqGenes.to_excel(writer, sheet_name='GOtermsSummaryCounts_Pop')
    FilterByOvClinVarUniqDrugGenes.to_excel(writer, sheet_name='DrugGenePairFromOvClinVar_Pop')
    FilterByOvGOUniqDrugGenes.to_excel(writer, sheet_name='DrugGenePairFromOvGO_Pop')
    summary1.to_excel(writer, sheet_name='Pop_FilterByOvClinVar')
    summary2.to_excel(writer, sheet_name='Pop_FilterByOvGO')


# #### To export full script run report click 'File' tab --> click 'Download as' --> click 'HTML (.html)'. The file will get downloaded in the default download folder
# 
# #### To export full script run report as 'pdf' click 'File' tab --> click 'Download as' --> click 'PDF via LaTex(.pdf)'. The file will get downloaded in the default download folder. Even after installing LaTex the direct download as pdf may not work. So an alternative way is, to download as .html first as describe above and then use online tool like  https://html2pdf.com/ to convert .html to .pdf. 
# 
# #### exit notebook by click 'File' tab --> click 'Close and Halt'
# 
# #### click 'Logout' in home to close jupyter notebook
# 
# #### (optional) close Anaconda Navigator
# 
# # End
