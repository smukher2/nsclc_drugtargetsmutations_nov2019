############################################################################################################################  
							#HELLO  							 
############################################################################################################################  

#NSCLC_DrugTargetsMutations_Nov2019  
#title: "LungCancer_BELnetwork_Nov2019"  
#author: "Shradha Mukherjee"  
#date created: "August 4, 2019"  
#date last updated: "October 10, 2019"  
#submission: Initial Submission "November 4, 2019"  
#Citation: Shradha Mukherjee. COPY OF MY MASTERS (MS) THESIS AS AVAILABLE IN LIBRARY OF ARIZONA STATE UNIVERSITY, U.S.: Targeted BEL network representation and characterization of commonly mutated genes in Non-Small Cell Lung Carcinoma (NSCLC). HAL Open Science Archive 2019. ⟨hal-04084202⟩ https://hal.science/hal-04084202  

############################################################################################################################  
						     #SETUP  							 
############################################################################################################################  

#####Install Anaconda on your Mac or Linux Operating System#####  
https://docs.anaconda.com/anaconda/install/index.html    
    

######Create conda environment for Python, IDEs Spyder and Jupyter Notebook#####  
#here env name being created is 'rpyide9', please use any name you like. Here 3.11 version is specified, specify whatever version is latest when you run  
conda create --name rpyide9 python=3.11  
conda activate rpyide9  
conda config --add channels conda-forge --env  
conda config --set channel_priority strict --env  
conda config --get channels --env  
#now install package or librarie by typing following on Terminal commandline, after updating all packages in the environment  
conda update --all --name rpyide9  
conda install spyder --name rpyide9  
conda install jupyter-offlinenotebook --name rpyide9  
#install packages directly on commanline Terminal no need to enter python interpretor or spyder or jupyter notebook  
conda install numpy pandas matplotlib seaborn scikit-learn xgboost --name rpyide9  
#Type following to exit conda env  
conda deactivate  
  

############################################################################################################################  
						      #USE  							  							
############################################################################################################################  
  

#These python scripts are used to ‘capture’, 'filter' and make 'machine readable' relevant biological knowledge.  

######Commandline Usage######    
#For .py or .ipynb activate rpyide9  
conda activate rpyide9  
cd PbOthers_RNAseq_ML_Feb2023  
#To run .py script  
python3 script_filename.py  
  

######IDE Usage######  
#For .py or .ipynb activate rpyide9  
conda activate rpyide9  
cd PbOthers_RNAseq_ML_Feb2023  
#To run .ipynb in IDE start and use jupyter notebook, use jupyter notebook gui to navigate/make 'PbOthers_RNAseq_ML_Feb2023' the working directory  
jupyter notebook  
#Click File > quit to exit and close jupyter notebook  
#To run .py in IDE start and use spyder, use spyder gui to navigate/make 'PbOthers_RNAseq_ML_Feb2023' the working directory  
spyder  
#Click File > quit to exit and close spyder  
#Type following to exit conda env  
conda deactivate  
  

############################################################################################################################  
								#THANK YOU  								
############################################################################################################################  

